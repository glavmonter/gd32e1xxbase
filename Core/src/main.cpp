#include <SEGGER_RTT.h>
#include <gd32e10x.h>
#include <rtt_log.h>
#include <usbd_int.h>
#include "usb_drv.h"
#include "cdc_acm_core.h"
#include "usb_delay.h"
#include "main_app.hpp"
#include <FreeRTOS.h>
#include <task.h>

uint8_t timer_prescaler = 0;
uint32_t usbfs_prescaler = 0;
static void usb_clock_config(void);
static void usb_interrupt_config(void);

#if configGENERATE_RUN_TIME_STATS == 1
volatile unsigned long ulRunTimeStatsClock = 0;
static void setupRunTimeStats();
#endif

//static uint8_t HidBuffer[64];

int main(int argc, char* argv[]) {
(void)argc;
(void)argv;
    SystemCoreClockUpdate();


    if (CONFIG_LOG_MAXIMUM_LEVEL > RTT_LOG_NONE) {
        DWT->CYCCNT = 0;
        DWT->CTRL |= 1;
        SEGGER_RTT_ConfigUpBuffer(0, nullptr, nullptr, 0, SEGGER_RTT_MODE_NO_BLOCK_SKIP);
        rtt_log_set_vprintf([](const char *sFormat, va_list va) { return SEGGER_RTT_vprintf(0, sFormat, &va); });
    }
    RTT_LOGI("MAIN", "Init!!");
    RTT_LOGI("MAIN", "VTOR: 0x%08lX", SCB->VTOR);
    /* configure USB clock */
    usb_clock_config();

    /* timer nvic initialization */
    timer_nvic_init();

    /* USB device stack configure */
    usbd_init(&usbfs_core_dev, USB_FS_CORE_ID);

    /* USB interrupt configure */
    usb_interrupt_config();

    /* check if USB device is enumerated successfully */
    while (usbfs_core_dev.dev.status != USB_STATUS_CONFIGURED) {}

    /* delay 10 ms */
    if(NULL != usbfs_core_dev.mdelay){
        usbfs_core_dev.mdelay(10);
    }

    for (;;) {
        if (USB_STATUS_CONFIGURED == usbfs_core_dev.dev.status) {
            if ((1 == packet_receive) && (1 == packet_sent)) {
                packet_sent = 0;
                /* receive datas from the host when the last packet datas have sent to the host */
                cdc_acm_data_receive(&usbfs_core_dev);
            } else {
                if (0 != receive_length) {
                    /* send receive datas */
                    cdc_acm_data_send(&usbfs_core_dev, receive_length);
                    receive_length = 0;
                }
            }
        }
    }

    NVIC_SetPriorityGrouping(0);
    InitApp();
#if configGENERATE_RUN_TIME_STATS == 1
    setupRunTimeStats();
#endif
    vTaskStartScheduler();
    return 0;
}

void usb_clock_config(void)
{
    uint32_t system_clock = rcu_clock_freq_get(CK_SYS);

    if (system_clock == 48000000) {
        usbfs_prescaler = RCU_CKUSB_CKPLL_DIV1;
        timer_prescaler = 3;
    } else if (system_clock == 72000000) {
        usbfs_prescaler = RCU_CKUSB_CKPLL_DIV1_5;
        timer_prescaler = 5;
    } else if (system_clock == 120000000) {
        usbfs_prescaler = RCU_CKUSB_CKPLL_DIV2_5;
        timer_prescaler = 9;
    } else {
        /*  reserved  */
    }

    rcu_usb_clock_config(usbfs_prescaler);
    rcu_periph_clock_enable(RCU_USBFS);
}

void usb_interrupt_config(void)
{
    nvic_priority_group_set(NVIC_PRIGROUP_PRE2_SUB2);
    nvic_irq_enable((uint8_t)USBFS_IRQn, 2U, 0U);

    /* enable the power module clock */
    rcu_periph_clock_enable(RCU_PMU);

    /* USB wakeup EXTI line configuration */
    exti_interrupt_flag_clear(EXTI_18);
    exti_init(EXTI_18, EXTI_INTERRUPT, EXTI_TRIG_RISING);
    exti_interrupt_enable(EXTI_18);

    nvic_irq_enable((uint8_t)USBFS_WKUP_IRQn, 1U, 0U);
}

#if configGENERATE_RUN_TIME_STATS == 1
void setupRunTimeStats() {
    // TIMER2, 20 кГц IRQ
    MDR_TIMER_TypeDef *tmr = nullptr;
    IRQn_Type tmr_irqn = HardFault_IRQn;
    if (configRUNTIME_STATS_TIMER == MDR_TIMER1) {
        RST_CLK_PCLKcmd(RST_CLK_PCLK_TIMER1, ENABLE);
        tmr = MDR_TIMER1;
        tmr_irqn = Timer1_IRQn;
    } else if (configRUNTIME_STATS_TIMER == MDR_TIMER2) {
        RST_CLK_PCLKcmd(RST_CLK_PCLK_TIMER2, ENABLE);
        tmr = MDR_TIMER2;
        tmr_irqn = Timer2_IRQn;
    } else if (configRUNTIME_STATS_TIMER == MDR_TIMER3) {
        RST_CLK_PCLKcmd(RST_CLK_PCLK_TIMER3, ENABLE);
        tmr = MDR_TIMER3;
        tmr_irqn = Timer3_IRQn;
    }
    assert_param(tmr);

    TIMER_DeInit(tmr);
    TIMER_BRGInit(tmr, TIMER_HCLKdiv1); // Включаем тактовую на таймер
    tmr->CNTRL = 0;
    tmr->CNT = 0;
    tmr->PSG = 0;
    tmr->ARR = SystemCoreClock / 20000 - 1;
    tmr->IE = TIMER_IE_CNT_ARR_EVENT_IE;
    tmr->CNTRL = TIMER_CNTRL_CNT_EN;
    NVIC_SetPriority(tmr_irqn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 2, 0));
    NVIC_EnableIRQ(tmr_irqn);
}

extern "C" void configRUNTIME_STATS_TIMER_IRQ() {
    if (configRUNTIME_STATS_TIMER->STATUS & TIMER_STATUS_CNT_ARR) {
        ulRunTimeStatsClock++;
    }
    configRUNTIME_STATS_TIMER->STATUS = 0;
}
#endif

//extern usb_core_handle_struct usbfs_core_dev;
extern "C" void USBFS_IRQHandler() {
    usbd_isr(&usbfs_core_dev);
}

extern uint32_t usbfs_prescaler;
extern "C" void USBFS_WKUP_IRQHandler() {
    if (usbfs_core_dev.cfg.low_power) {
        SystemInit();
        rcu_usb_clock_config(usbfs_prescaler);
        rcu_periph_clock_enable(RCU_USBFS);
        usb_clock_ungate(&usbfs_core_dev);
    }
    exti_interrupt_flag_clear(EXTI_18);
}

extern "C" void TIMER0_UP_TIMER9_IRQHandler() {
    timer_delay_irq();
}
