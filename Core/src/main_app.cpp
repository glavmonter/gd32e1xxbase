#include <cstring>
#include <gd32e10x.h>

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include "errors.h"
#include "main_app.hpp"


#include <thread.hpp>
#include <ticks.hpp>
#include <queue.hpp>

#include "log_levels.h"
#include <rtt_log.h>

static const char *TAG_MAIN = "MAIN";
static const char *TAG_BLINK = "BLINK";
static const char *TAG_PORT = "PORT";
static const char *TAG_CONSUMER = "CONSUMER";
static const char *TAG_PRODUCER = "PRODUCER";

static void InitTimerAndPort();


class ConsumerThread : public cpp_freertos::Thread {
public:
    ConsumerThread(const char *name, cpp_freertos::Queue &queue) :
        cpp_freertos::Thread(name, configMINIMAL_STACK_SIZE * 2, tskIDLE_PRIORITY),
        m_xQueue(queue) {
    }

protected:
    void Run() override {
        RTT_LOGI(TAG_CONSUMER, "Run Consumer!!");
        uint32_t rcv;
        for (;;) {
            if (m_xQueue.Dequeue(&rcv)) {
                RTT_LOGI(TAG_CONSUMER, "Rcv: %lu", rcv);
            }
        }
    }

private:
    cpp_freertos::Queue &m_xQueue;
};


class ProducerThread : public cpp_freertos::Thread {
public:
    ProducerThread(const char *name, uint32_t index, TickType_t period, cpp_freertos::Queue &queue) :
        cpp_freertos::Thread(name, configMINIMAL_STACK_SIZE * 2, configMAX_PRIORITIES - 1),
        m_iSendIndex(index), m_iPeriod(period), m_xQueue(queue) {
    }

protected:
    void Run() override {
        for (;;) {
            Delay(cpp_freertos::Ticks::MsToTicks(m_iPeriod));
            m_xQueue.Enqueue(&m_iSendIndex);
        }
    }

private:
    uint32_t m_iSendIndex;
    TickType_t m_iPeriod;
    cpp_freertos::Queue &m_xQueue;
};


cpp_freertos::Queue queue{2, sizeof(uint32_t)};
ConsumerThread consumerThread{"Consumer", queue};
ProducerThread producerThread1{"Prod1", 0, 200, queue};
ProducerThread producerThread2{"Prod2", 1, 300, queue};

void InitApp() {
    consumerThread.Start();
    producerThread1.Start();
    producerThread2.Start();
}
