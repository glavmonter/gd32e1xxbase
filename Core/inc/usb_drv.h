//===============================================================================
// File             : usb_drv.h
// Date             : 04.10.21
// Notes            :
//===============================================================================


/**** Global definitions */

#include <usb_core.h>
#include "usbd_conf.h"

/**** External structures */

extern usb_core_handle_struct usbfs_core_dev;


/**** External variables */

extern uint8_t packet_sent, packet_receive;
extern uint32_t receive_length;
extern uint8_t usb_data_buffer[CDC_ACM_DATA_PACKET_SIZE];


/**** External functions */

#ifdef __cplusplus
extern "C" {
#endif

void usb_drv_Init(void);

#ifdef __cplusplus
} // extern "C"
#endif




