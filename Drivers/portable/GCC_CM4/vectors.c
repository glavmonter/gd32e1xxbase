#include "exception_handlers.h"

void Default_Handler (void) __attribute__((weak));

/* GD32E10x Specific Interrupts */
void WWDGT_IRQHandler                   (void) __attribute__ ((weak, alias("Default_Handler")));
void LVD_IRQHandler                     (void) __attribute__ ((weak, alias("Default_Handler")));
void TAMPER_IRQHandler                  (void) __attribute__ ((weak, alias("Default_Handler")));
void RTC_IRQHandler                     (void) __attribute__ ((weak, alias("Default_Handler")));
void FMC_IRQHandler                     (void) __attribute__ ((weak, alias("Default_Handler")));
void RCU_CTC_IRQHandler                 (void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI0_IRQHandler                   (void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI1_IRQHandler                   (void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI2_IRQHandler                   (void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI3_IRQHandler                   (void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI4_IRQHandler                   (void) __attribute__ ((weak, alias("Default_Handler")));
void DMA0_Channel0_IRQHandler           (void) __attribute__ ((weak, alias("Default_Handler")));
void DMA0_Channel1_IRQHandler           (void) __attribute__ ((weak, alias("Default_Handler")));
void DMA0_Channel2_IRQHandler           (void) __attribute__ ((weak, alias("Default_Handler")));
void DMA0_Channel3_IRQHandler           (void) __attribute__ ((weak, alias("Default_Handler")));
void DMA0_Channel4_IRQHandler           (void) __attribute__ ((weak, alias("Default_Handler")));
void DMA0_Channel5_IRQHandler           (void) __attribute__ ((weak, alias("Default_Handler")));
void DMA0_Channel6_IRQHandler           (void) __attribute__ ((weak, alias("Default_Handler")));
void ADC0_1_IRQHandler                  (void) __attribute__ ((weak, alias("Default_Handler")));
void CAN0_TX_IRQHandler                 (void) __attribute__ ((weak, alias("Default_Handler")));
void CAN0_RX0_IRQHandler                (void) __attribute__ ((weak, alias("Default_Handler")));
void CAN0_RX1_IRQHandler                (void) __attribute__ ((weak, alias("Default_Handler")));
void CAN0_EWMC_IRQHandler               (void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI5_9_IRQHandler                 (void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER0_BRK_TIMER8_IRQHandler       (void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER0_UP_TIMER9_IRQHandler        (void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER0_TRG_CMT_TIMER10_IRQHandler  (void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER0_Channel_IRQHandler          (void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER1_IRQHandler                  (void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER2_IRQHandler                  (void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER3_IRQHandler                  (void) __attribute__ ((weak, alias("Default_Handler")));
void I2C0_EV_IRQHandler                 (void) __attribute__ ((weak, alias("Default_Handler")));
void I2C0_ER_IRQHandler                 (void) __attribute__ ((weak, alias("Default_Handler")));
void I2C1_EV_IRQHandler                 (void) __attribute__ ((weak, alias("Default_Handler")));
void I2C1_ER_IRQHandler                 (void) __attribute__ ((weak, alias("Default_Handler")));
void SPI0_IRQHandler                    (void) __attribute__ ((weak, alias("Default_Handler")));
void SPI1_IRQHandler                    (void) __attribute__ ((weak, alias("Default_Handler")));
void USART0_IRQHandler                  (void) __attribute__ ((weak, alias("Default_Handler")));
void USART1_IRQHandler                  (void) __attribute__ ((weak, alias("Default_Handler")));
void USART2_IRQHandler                  (void) __attribute__ ((weak, alias("Default_Handler")));
void EXTI10_15_IRQHandler               (void) __attribute__ ((weak, alias("Default_Handler")));
void RTC_Alarm_IRQHandler               (void) __attribute__ ((weak, alias("Default_Handler")));
void USBFS_WKUP_IRQHandler              (void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER7_BRK_TIMER11_IRQHandler      (void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER7_UP_TIMER12_IRQHandler       (void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER7_TRG_CMT_TIMER13_IRQHandler  (void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER7_Channel_IRQHandler          (void) __attribute__ ((weak, alias("Default_Handler")));
void EXMC_IRQHandler                    (void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER4_IRQHandler                  (void) __attribute__ ((weak, alias("Default_Handler")));
void SPI2_IRQHandler                    (void) __attribute__ ((weak, alias("Default_Handler")));
void UART3_IRQHandler                   (void) __attribute__ ((weak, alias("Default_Handler")));
void UART4_IRQHandler                   (void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER5_IRQHandler                  (void) __attribute__ ((weak, alias("Default_Handler")));
void TIMER6_IRQHandler                  (void) __attribute__ ((weak, alias("Default_Handler")));
void DMA1_Channel0_IRQHandler           (void) __attribute__ ((weak, alias("Default_Handler")));
void DMA1_Channel1_IRQHandler           (void) __attribute__ ((weak, alias("Default_Handler")));
void DMA1_Channel2_IRQHandler           (void) __attribute__ ((weak, alias("Default_Handler")));
void DMA1_Channel3_IRQHandler           (void) __attribute__ ((weak, alias("Default_Handler")));
void DMA1_Channel4_IRQHandler           (void) __attribute__ ((weak, alias("Default_Handler")));
void CAN1_TX_IRQHandler                 (void) __attribute__ ((weak, alias("Default_Handler")));
void CAN1_RX0_IRQHandler                (void) __attribute__ ((weak, alias("Default_Handler")));
void CAN1_RX1_IRQHandler                (void) __attribute__ ((weak, alias("Default_Handler")));
void CAN1_EWMC_IRQHandler               (void) __attribute__ ((weak, alias("Default_Handler")));
void USBFS_IRQHandler                   (void) __attribute__ ((weak, alias("Default_Handler")));


extern unsigned int __stack;

typedef void (*const pHandler)(void);

// The vector table.
// This relies on the linker script to place at correct location in memory.

pHandler __isr_vectors[] __attribute__ ((section(".isr_vector"),used)) =  {
        (pHandler) &__stack,                    // The initial stack pointer
        Reset_Handler,                          // The reset handler

        NMI_Handler,                            // The NMI handler
        HardFault_Handler,                      // The hard fault handler

#if defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
        MemManage_Handler,                      // The MPU fault handler
        BusFault_Handler,                       // The bus fault handler
        UsageFault_Handler,                     // The usage fault handler
#else
        0, 0, 0,				                // Reserved
#endif
        0,                                      // Reserved
        0,                                      // Reserved
        0,                                      // Reserved
        0,                                      // Reserved
        SVC_Handler,                            // SVCall handler
#if defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
        DebugMon_Handler,                       // Debug monitor handler
#else
        0,					                    // Reserved
#endif
        0,                                      // Reserved
        PendSV_Handler,                         // The PendSV handler
        SysTick_Handler,                        // The SysTick handler

		/* External interrupts */
        WWDGT_IRQHandler,                       /* 16: Window Watchdog Timer                         */
        LVD_IRQHandler,                         /* 17: LVD through EXTI Line detect                  */
        TAMPER_IRQHandler,                      /* 18: Tamper through EXTI Line detect               */
        RTC_IRQHandler,                         /* 19: RTC through EXTI Line                         */
        FMC_IRQHandler,                         /* 20: FMC                                           */
        RCU_CTC_IRQHandler,                     /* 21: RCU and CTC                                   */
        EXTI0_IRQHandler,                       /* 22: EXTI Line 0                                   */
        EXTI1_IRQHandler,                       /* 23: EXTI Line 1                                   */
        EXTI2_IRQHandler,                       /* 24: EXTI Line 2                                   */
        EXTI3_IRQHandler,                       /* 25: EXTI Line 3                                   */
        EXTI4_IRQHandler,                       /* 26: EXTI Line 4                                   */
        DMA0_Channel0_IRQHandler,               /* 27: DMA0 Channel0                                 */
        DMA0_Channel1_IRQHandler,               /* 28: DMA0 Channel1                                 */
        DMA0_Channel2_IRQHandler,               /* 29: DMA0 Channel2                                 */
        DMA0_Channel3_IRQHandler,               /* 30: DMA0 Channel3                                 */
        DMA0_Channel4_IRQHandler,               /* 31: DMA0 Channel4                                 */
        DMA0_Channel5_IRQHandler,               /* 32: DMA0 Channel5                                 */
        DMA0_Channel6_IRQHandler,               /* 33: DMA0 Channel6                                 */
        ADC0_1_IRQHandler,                      /* 34: ADC0 and ADC1                                 */
        CAN0_TX_IRQHandler,                     /* 35: CAN0 TX                                       */
        CAN0_RX0_IRQHandler,                    /* 36: CAN0 RX0                                      */
        CAN0_RX1_IRQHandler,                    /* 37: CAN0 RX1                                      */
        CAN0_EWMC_IRQHandler,                   /* 38: CAN0 EWMC                                     */
        EXTI5_9_IRQHandler,                     /* 39: EXTI5 to EXTI9                                */
        TIMER0_BRK_TIMER8_IRQHandler,           /* 40: TIMER0 Break and TIMER8                       */
        TIMER0_UP_TIMER9_IRQHandler,            /* 41: TIMER0 Update and TIMER9                      */
        TIMER0_TRG_CMT_TIMER10_IRQHandler,      /* 42: TIMER0 Trigger and Commutation and TIMER10    */
        TIMER0_Channel_IRQHandler,              /* 43: TIMER0 Channel Capture Compare                */
        TIMER1_IRQHandler,                      /* 44: TIMER1                                        */
        TIMER2_IRQHandler,                      /* 45: TIMER2                                        */
        TIMER3_IRQHandler,                      /* 46: TIMER3                                        */
        I2C0_EV_IRQHandler,                     /* 47: I2C0 Event                                    */
        I2C0_ER_IRQHandler,                     /* 48: I2C0 Error                                    */
        I2C1_EV_IRQHandler,                     /* 49: I2C1 Event                                    */
        I2C1_ER_IRQHandler,                     /* 50: I2C1 Error                                    */
        SPI0_IRQHandler,                        /* 51: SPI0                                          */
        SPI1_IRQHandler,                        /* 52: SPI1                                          */
        USART0_IRQHandler,                      /* 53: USART0                                        */
        USART1_IRQHandler,                      /* 54: USART1                                        */
        USART2_IRQHandler,                      /* 55: USART2                                        */
        EXTI10_15_IRQHandler,                   /* 56: EXTI10 to EXTI15                              */
        RTC_Alarm_IRQHandler,                   /* 57: RTC Alarm                                     */
        USBFS_WKUP_IRQHandler,                  /* 58: USBFS Wakeup                                  */
        TIMER7_BRK_TIMER11_IRQHandler,          /* 59: TIMER7 Break and TIMER11                      */
        TIMER7_UP_TIMER12_IRQHandler,           /* 60: TIMER7 Update and TIMER12                     */
        TIMER7_TRG_CMT_TIMER13_IRQHandler,      /* 61: TIMER7 Trigger and Commutation and TIMER13    */
        TIMER7_Channel_IRQHandler,              /* 62: TIMER7 Channel Capture Compare                */
        0,                                      /* 63: Reserved                                      */
        EXMC_IRQHandler,                        /* 64: EXMC                                          */
        0,                                      /* 65: Reserved                                      */
        TIMER4_IRQHandler,                      /* 66: TIMER4                                        */
        SPI2_IRQHandler,                        /* 67: SPI2                                          */
        UART3_IRQHandler,                       /* 68: UART3                                         */
        UART4_IRQHandler ,                      /* 69: UART4                                         */
        TIMER5_IRQHandler,                      /* 70: TIMER5                                        */
        TIMER6_IRQHandler,                      /* 71: TIMER6                                        */
        DMA1_Channel0_IRQHandler,               /* 72: DMA1 Channel0                                 */
        DMA1_Channel1_IRQHandler,               /* 73: DMA1 Channel1                                 */
        DMA1_Channel2_IRQHandler,               /* 74: DMA1 Channel2                                 */
        DMA1_Channel3_IRQHandler,               /* 75: DMA1 Channel3                                 */
        DMA1_Channel4_IRQHandler,               /* 76: DMA1 Channel4                                 */
        0,                                      /* 77: Reserved                                      */
        0,                                      /* 78: Reserved                                      */
        CAN1_TX_IRQHandler,                     /* 79: CAN1 TX                                       */
        CAN1_RX0_IRQHandler,                    /* 80: CAN1 RX0                                      */
        CAN1_RX1_IRQHandler,                    /* 81: CAN1 RX1                                      */
        CAN1_EWMC_IRQHandler,                   /* 82: CAN1 EWMC                                     */
        USBFS_IRQHandler                        /* 83: USBFS                                         */
};

// Processor ends up here if an unexpected interrupt occurs or a specific
// handler is not present in the application code.
__attribute__ ((section(".after_vectors")))
void Default_Handler (void)
{
	while (1) ;
}
